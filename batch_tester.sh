#!/bin/bash

javadir="$HOME/alchemist/jdk1.8.0_121/bin"
CP="data/alchemist-redist-7.0.1.jar"

function usage() {
    echo -e "\033[4mcommands and parameters:\033[0m"
    echo -e "    \033[1mall\033[0m:                             fully builds all experiments"
    echo -e "       [node]"
    echo -e "    \033[1mhelp\033[0m:                            show alchemist help"
    echo -e "    \033[1mgui\033[0m:                             run program in alchemist gui"
    echo -e "       <prog>"
    echo -e "    \033[1mcompile\033[0m:                         compile files into \"bin\" directory"
    echo -e "    \033[1mclean\033[0m:                           clean files produced by experiment"
    echo -e "       <prog>"
    echo -e "    \033[1mplot\033[0m:                            build resuming plots"
    echo -e "       [node] <prog> <plots...>"
    echo -e "    \033[1mprogress\033[0m:                        track progress of a running process and plots"
    echo -e "       [node] <prog> [points] <plots...>"
    echo -e "    \033[1mrun\033[0m:                             run batch tests, tracks and plots"
    echo -e "       [nodes...] <prog> <step> <endtime> <plots...> [variables]"
    exit 0
}

if ! which free >/dev/null 2>/dev/null; then
    function free() {
        echo "Mem:          0           0"
        echo "Swap:         0           0"
    }
fi
if ! which javac >/dev/null 2>/dev/null; then
    function java() {
        $javadir/java "$@"
    }
    function javac() {
        $javadir/javac "$@"
    }
fi
if ! which occam-run > /dev/null; then
    function occam-run() {
        node=`echo $2 | tr -cd '0-9'`
        file="node$node-$RANDOM"
        shift 3
        if [[ $1 =~ ^[0-9]+$ ]]; then
            xmx="-Xmx$1g"
            shift 1
        fi
        if [ "${1:0:1}" == "-" ]; then
            nohup time -p java $xmx -cp "bin:$CP" it.unibo.alchemist.Alchemist "$@" 1>>$file.log 2>>$file.log &
        else
            "$@" 1>>$file.log 2>>$file.log
        fi
        echo "Remote ID (file where output is stored): $file"
    }
fi


if [ "$1" == "" ]; then
    usage
fi

me="$0"
cmd="$1"
shift 1

if [ "$cmd" == "all" ]; then
    if [ "$2" != "" ]; then
        usage
    fi
    if [ "$1" != "" ]; then
        if [[ "$1" =~ ^[0-9]+$ ]]; then
            node=$1
        else
            usage
        fi
    fi
    if [ -t 1 ]; then
      nohup "$me" all "$@" 1>>"all.log" 2>>"all.log" &
      exit 0
    fi
    echo "Isolation Test..."
    "$me" run $node performance 10  800   "variability@time:(err)+(diff)"                      variability noise random
    while [ `echo performance*.log` == "performance*.log" ]; do
      sleep 5
    done
    while [ `cat performance*.log | grep FINISHED | wc -l` -eq 0 ]; do
      sleep 5
    done
    echo "Channel Broadcasting Test..."
    "$me" run $node channel     10  800   "variability@time:(err)+(dc)"                        variability random
    while [ `echo channel*.log` == "channel*.log" ]; do
      sleep 5
    done
    while [ `cat channel*.log | grep FINISHED | wc -l` -eq 0 ]; do
      sleep 5
    done
    echo "Data Collection Test..."
    "$me" run $node collection  10  800   "variability@time:noise=0(val@50)+noise=0.5(val@50)" variability noise random
    while [ `echo collection*.log` == "collection*.log" ]; do
      sleep 5
    done
    while [ `cat collection*.log | grep FINISHED | wc -l` -eq 0 ]; do
      sleep 5
    done
    echo "Done!"
    exit 0
    echo "Case Study..."
    "$me" run $node study       300 30000 "time:(ratio*algo)+(wait*algo@m&100)"                algo random
fi

if [ "$cmd" == "help" ]; then
    if [ "$1" != "" ]; then
        usage
    fi
    java -cp "$CP" it.unibo.alchemist.Alchemist -h
    exit 0
fi

if [ "$cmd" == "gui" ]; then
    if [ "$1" == "" -o "$2" != "" ]; then
        usage
    fi
    prog="$1"
    java -cp "bin:$CP" it.unibo.alchemist.Alchemist -y src/main/yaml/"$prog".yml -g src/main/resources/effects.aes
    exit 0
fi

if [ "$cmd" = "compile" ]; then
    if [ "$1" != "" ]; then
        usage
    fi
    if [ ! -d "bin" ]; then
        mkdir bin
    fi
    for i in src/main/protelis/* src/main/resources/*; do
        j=`basename "$i"`
        if [ ! -f "bin/$j" ]; then
            ln -s "../$i" "bin/$j"
        fi
    done
    javac `find src/main/java/ -name "*.java"` -d bin/ -cp "$CP"
    exit 0
fi

if [ "$cmd" == "clean" ]; then
    if [ "$1" == "" -o "$2" != "" ]; then
        usage
    fi
    prog="$1"
    if [ ! -d TRASH ]; then
        mkdir TRASH
    fi
    mv "data/raw/$prog"?* TRASH/
    mv "$prog"*.log TRASH/
    rm node*
    exit 0
fi


nodes=( )
while [[ "${1:0:1}${1%????}" =~ ^[0-9]+$ ]]; do
    nodes=("${nodes[@]}" "$1")
    shift 1
done
if [ "${#nodes[@]}" -eq 0 ]; then
    nodes=( 0 )
fi
if [ "$1" == "" ]; then
    usage
fi
prog="$1"
shift 1
if [ "${#nodes[@]}" -eq 1 ]; then
    node=`echo $nodes | tr -cd [:digit:]`
    fold="$prog$nodes"
    path="data/raw/$fold"
    files="$path/$prog"
elif [ "$cmd" != "run" ]; then
    usage
fi


if [ "$cmd" == "plot" ]; then
    if [ "$1" == "" ]; then
        usage
    fi
    file=`ls "$files"* | head -n 1`
    for ((k=0; k<1000; k++)); do
        if [ ! -e "data/$prog.v$k.txt" ]; then break; fi
        if [ "$file" -ot "data/$prog.v$k.txt" ]; then break; fi
    done
    file="data/$prog.v$k.txt"
    ./plot_builder.py "$files"* "$@" > "$file"
    exit 0
fi

if [ "$cmd" == "progress" ]; then
    points="10"
    if [[ "$1" =~ ^[0-9]+$ ]]; then
	points="$1"
	shift 1
    fi
    while [ "`echo "$files"*`" == "$files*" ]; do
        sleep 5
    done
    perc="000"
    maxram=0
    maxswap=0
    totram=0
    totswap=0
    ram=0
    swap=0
    while [ ${perc:0:3} -lt 100 ]; do
        t=`./plot_builder.py $points "$files"* 2>/dev/null`
        while [ "${t:0:3}" == "${perc:0:3}" ]; do
            sleep 5
            t=`./plot_builder.py $points "$files"* 2>/dev/null`
        done
        perc="$t"
        file=`occam-run -n node$node sdonetti/alchemist free -m | grep "Remote ID" | sed 's|.* ||'`
        if [ "$file" != "" ]; then
            while [ "`echo "$file".log*`" == "$file.log*" ]; do
                sleep 5
            done
            sleep 10
            totram=`cat "$file.log" | grep "Mem" | tr -s ' ' | cut -d ' ' -f 2`
            ram=`cat "$file.log" | grep "Mem" | tr -s ' ' | cut -d ' ' -f 3`
            totswap=`cat "$file.log" | grep "Swap" | tr -s ' ' | cut -d ' ' -f 2`
            swap=`cat "$file.log" | grep "Swap" | tr -s ' ' | cut -d ' ' -f 3`
            totram=$[totram/1024]
            ram=$[ram/1024]
            rm "$file".*
        fi
        maxram=$[maxram>ram?maxram:ram]
        maxswap=$[maxswap>swap?maxswap:swap]
        echo "$perc  (`date`), $ram/$totram GB ram - $swap/$totswap MB swap"
    done
    if [ "$1" != "" ]; then
        "$me" plot "$nodes" "$prog" "$@"
    fi
    echo "FINISHED: $maxram/$totram GB ram - $maxswap/$totswap MB swap"
    exit 0
fi

if [ "$cmd" == "run" ]; then
    step="$1"
    time="$2"
    shift 2
    plots=( )
    while [ "${1: -1}" == ")" ]; do
        plots=("${plots[@]}" "$1")
        shift 1
    done
    if [ "${#nodes[@]}" -gt 1 ]; then
        if [ -t 1 ]; then
            nohup "$me" run "${nodes[@]}" "$prog" "$step" "$time" "${plots[@]}" "$@" 1>>"$prog.log" 2>>"$prog.log" &
            exit 0
        fi
        folders=( )
        echo "RUNNING..."
        for n in "${nodes[@]}"; do
            count=`for i in ${nodes[@]}; do echo $i; done | sort | uniq -c | grep "$n$" | sed -E 's| [0-9]*$||'`
            if [ "$count" -gt 1 ]; then
                code=`LC_CTYPE=C tr -cd '[:lower:]' < /dev/urandom | fold -w4 | head -n1`
                node=$n$code
            else
                node=$n
            fi
            folders=("${folders[@]}" "data/raw/$prog$node")
            "$me" run "$node" "$prog" "$step" "$time" "$@"
        done
        echo "WAITING..."
        while [ `cat "$prog"*.log | grep FINISHED | wc -l` -lt "${#nodes[@]}" ]; do
            sleep 10
        done
        echo "SYNCHRONIZING..."
        yes | ./comparator.sh "${folders[@]}" "${plots[@]}"
        echo "PLOTTING..."
        "$me" plot "$prog" "${plots[@]}"
        echo "POLISHING..."
        "$me" clean "$prog"
        echo "DONE!"
        exit 0
    fi
    if   [ "$node" == "12" ]; then
        xmx="40"
        par="-p 16"
    elif [ "$node" == "35" ]; then
        xmx="512"
        par="-p 32"
    fi
    if [ "$1" != "" -a "${1:0:1}" != "-" ]; then
        extra="-b -var"
    fi

    if [ ! -d "data/raw" ]; then
        mkdir data/raw
    fi
    if [ ! -d "$path" ]; then
        mkdir "$path"
    fi
    if [ -f "$fold.log" ]; then
        echo -n "overwrite regular file $fold.log? (Y/n) "
        read x
        if [ "$x" != "n" -a "$x" != "N" ]; then
            echo -n > "$fold.log"
        fi
    fi

    "$me" compile
    occam-run -n node$node sdonetti/alchemist $xmx -y src/main/yaml/"$prog".yml -e "$files" $par -i "$step" -t "$time" $extra "$@"
    points=$[time/step]
    nohup "$me" progress "$nodes" "$prog" $points "${plots[@]}" 1>>"$fold.log" 2>>"$fold.log" &
    exit 0
fi

usage
