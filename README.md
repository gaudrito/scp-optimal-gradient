# Optimal Single-Path Information Propagation in Gradient-based Algorithms

Submitted to [Science of Computer Programming](https://www.journals.elsevier.com/science-of-computer-programming/), extended version of [this paper](https://doi.org/10.1007/978-3-319-59746-1_4), best paper of [COORDINATION (DisCoTec 2017)](http://2017.discotec.org/). For any issues with reproducing the experiments, please contact [Giorgio Audrito](mailto:giorgio.audrito@unito.it).


## Description

This repository contains all the source code used to perform the experiments presented in the paper.


## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 8 JDK installed and set as default virtual machine
    - Eclipse Oxygen or above (older versions should work as well), if you want to inspect the code and execute the simulations in a GUI.

* Chrunching the generated data and producing the charts
    - Python 2.7.x installed and set as default Python interpreter
    - Maple 18 or above (older versions should work as well)
  
**Notes about the supported operating systems**

All the tests have been performed under Linux, no test has been performed under any other operating system, and we only support reproducing the tests under that environment.
Due to the portable nature of the used tools, the test suite should run also on other Unix-like OSs (e.g. BSD, MacOS X).


## Reproducing the experiment


### Importing the repository

The first step is cloning this repository. It can be easily done by using `git`. Open a terminal, move it into an empty folder of your choice, then issue the following command:

``git clone https://bitbucket.org/gaudrito/scp-optimal-gradient.git``

This should make all the required files appear in your repository, with the latest version.


### Executing the simulations

All the graphs for all simulations can be produced automatically by the command:

``./batch_tester.sh all``

issued from the root directory where the repository was cloned. The script is executed in background and saves output in the `data` folder as text files containing maple plotting code.
In case of problems with the fully-automated procedure, or in case you want to vary some parameters, it is also possible to produce them step by step in the following way.

* First, compiled java class files (from `src/main/java/`) and a copy of all files in directory `src/main/protelis/` need to be present in directory `bin/`. If you already run graphical simulations with Eclipse (see the next section), they should already be present. Otherwise, you can produce them with:  
  ``./batch_tester.sh compile``
  
* Then, you can produce the raw data associated to the experiments with the following commands:  
  ``./batch_tester.sh run performance 10 800 variability noise random``  
  ``./batch_tester.sh run channel 10 800 variability random``  
  ``./batch_tester.sh run collection 10 800 variability noise random``  
  During execution, progress can be tracked through `log` files named as the experiment names. Results will be written in directory `data/raw/`.

* Finally, plots can be extracted with the following commands:  
  ``./batch_tester.sh plot performance "variability@time:(err)+(diff)"``  
  ``./batch_tester.sh plot channel "variability@time:(err)+(dc)"``  
  ``./batch_tester.sh plot collection "variability@time:noise=0(val@50)+noise=0.5(val@50)"``  
  Results will be written in the `data/` directory into text files containing Maple source code able to produce the corresponding plots.

Different plots can be produced by varying the final string in the above commands. For a brief explanation of the plot descriptive syntax, issue the following command:

``./plot_builder.py``

In addition, this repository contains a copy of the case study in the [COORDINATION paper](https://doi.org/10.1007/978-3-319-59746-1_4), which can be reproduced through the following commands:

``./batch_tester.sh run study 300 30000 algo random``
``./batch_tester.sh plot study "time:(ratio*algo)+(wait*algo@m&100)"``

However, this experiment is not officially supported by this repository.


### Inspecting the code and simulation GUI

Open Eclipse, click on "File > Import" and then on "Gradle > Existing Gradle Project", then select the folder of the repository donwloaded in the previous step.

To properly visualize the source files you should also install the Protelis Parser through "Help > Install New Software" with the following address:

``http://efesto.apice.unibo.it/protelis-build/protelis-parser/protelis.parser.repository/target/repository/``

The files with the source code are in `src/main/protelis/` and contain the following:

* `utils.pt`: utility functions
* `gradients.pt`: code of all gradient algorithms
* `performance.pt`: code of the isolation test (as in Section 5.2)
* `channel.pt`: code of the channel broadcasting test (as in Section 5.3)
* `collection.pt`: code of the collection test (as in Section 5.4)
* `study.pt`: code of the case study (as in the [COORDINATION paper](https://doi.org/10.1007/978-3-319-59746-1_4))

The files describing the environment are in `src/main/yaml/` and contain the following:

* `performance.yaml`: environment used for the isolation test (as in Section 5.2)
* `channel.yaml`: environment used for the channel broadcasting test (as in Section 5.3)
* `collection.yaml`: environment used for the collection test (as in Section 5.4)
* `study.yaml`: environment used for the case study (as in the [COORDINATION paper](https://doi.org/10.1007/978-3-319-59746-1_4))

In the `java` folder, there is a `Math` class for checking geometric inclusions, and an `AlchemistExecutionContext` class which patches Alchemist by adding a function `distance(source, target)` (which is necessary to compute an ideal channel).
In the `resources` folder, there is an effects file for the GUI presentation.

In order to run the simulations with a GUI, create a Run Configuration in Eclipse with the following settings.

* Main class: `it.unibo.alchemist.Alchemist`
* Program arguments: `-g src/main/resources/effects.aes -y src/main/yaml/X.yml` where `X` can be either `performance`, `channel`, `collection`, or `study`
