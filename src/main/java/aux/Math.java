package aux;

public class Math {
	/**
	 * Inclusion check in geometric rectangle between a source and a destination.
	 * Mathematical formula:
	 * √(s²-r²) + √(d²-r²) <= t <==> 4(s²-r²)(d²-r²) <= (t² - (s²-r²) - (d²-r²))²
	 * where r = w/2 (6 sums, 6 multiplications).
	 * If any among s,d,t is infinite, it is not considered included.
	 * 
	 * @param s Distance from source
	 * @param d Distance from destination
	 * @param t Total distance from source and destination
	 * @param w Maximum width of the geometric shape.
	 * @return 1 if included, 0 otherwise
	 */
	public static int inRectangle(double s, double d, double t, double w) {
		if (s == Double.POSITIVE_INFINITY || d == Double.POSITIVE_INFINITY || t == Double.POSITIVE_INFINITY) return 0;
		w /= 2;
		s *= s; d *= d;
		t *= t; w *= w;
		s -= w; d -= w;
		t -= s+d;
		return 4*s*d <= t*t ? 1 : 0;
	}
	
	/**
	 * Inclusion check in geometric ellipse between a source and a destination.
	 * Mathematical formula:
	 * s + d <= √(t²+w²) <==> (s+d)² <= t²+w²
	 * (2 sums, 3 multiplications).
	 * If any among s,d,t is infinite, it is not considered included.
	 * 
	 * @param s Distance from source
	 * @param d Distance from destination
	 * @param t Total distance from source and destination
	 * @param w Maximum width of the geometric shape.
	 * @return 1 if included, 0 otherwise
	 */
	public static int inEllipse(double s, double d, double t, double w) {
		if (s == Double.POSITIVE_INFINITY || d == Double.POSITIVE_INFINITY || t == Double.POSITIVE_INFINITY) return 0;
		s += d;
		return s*s <= t*t + w*w ? 1 : 0;
	}
}
